import '../css/basic.css';
import React, { Component } from 'react';
import { Tile, ContentSwitcher, Switch } from 'carbon-components-react';
import { Navigation, 
  IllnessBar, CharityBubble, HospitalPie,
  CountryBar, IssueBubble, CharityPie } from '../components';

export class MyVis extends Component {
  constructor(props){
    super(props)
    this.state = {vis: 'bar'}
  }

  render(){
    let vis = this.state.vis === 'bar'? <IllnessBar/>: 
              this.state.vis === 'pie'? <HospitalPie/>: 
              this.state.vis === 'bubble'? <CharityBubble/>: 
              null;
    return (
      <div>
        <div className='navbar'>
          <Navigation selected={-1}/>
        </div>
        <div className='page-title'>
          <h1>Visualizations of our site</h1>
        </div>
        <ContentSwitcher className='vis-switcher' onChange={e => this.setState({vis: e.name})}>
          <Switch text="Illness Bar" name='bar'/>
          <Switch text="Charity Bubble" name='bubble'/>
          <Switch text="Hospital Pie" name='pie'/>
        </ContentSwitcher>

        <Tile className='vis-container'>{vis}</Tile>
      </div>
    )
  }
}

export class YourVis extends Component {
  constructor(props){
    super(props)
    this.state = {vis: 'bar'}
  }

  render(){
    let vis = this.state.vis === 'bar'? <CountryBar/>: 
              this.state.vis === 'bubble'? <IssueBubble/>: 
              this.state.vis === 'pie'? <CharityPie/>: 
              null;
    return (
      <div>
        <div className='navbar'>
          <Navigation selected={-1}/>
        </div>
        <div className='page-title'>
          <h1>Visualizations of the provider's site</h1>
        </div>
        <ContentSwitcher className='vis-switcher' onChange={e => this.setState({vis: e.name})}>
          <Switch text="Country Bar" name='bar'/>
          <Switch text="Issue Bubble" name='bubble'/>
          <Switch text="Charity Pie" name='pie'/>
        </ContentSwitcher>

        <Tile className='vis-container'>{vis}</Tile>
      </div>
    )
  }
}