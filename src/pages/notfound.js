import '../css/basic.css';
import React, { Component } from 'react';
import { Navigation } from '../components';

class NotFound extends Component{
  constructor(props){
    super(props);
    this.state = {timeout: 5}
    this.update = this.update.bind(this);
  }

  update() {this.setState(p => {return {timeout: p.timeout - 1}})}
  componentDidMount() {setTimeout(this.update, 1000);}
 
  componentDidUpdate(){
    if(this.state.timeout === 0)
      window.history.back();
    setTimeout(this.update, 1000);
  }

  render(){
    return(
      <div>
        <div className='navbar'>
          <Navigation selected={-1}/>
        </div>
        <div className='page-title'>
          <h1>Sorry, the page you are looking for does not exist.</h1><br/>
          <h2>Taking you back in {this.state.timeout}</h2>
        </div>
      </div>
    )
  }
}

export default NotFound;
