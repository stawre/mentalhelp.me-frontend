import "./css/App.css";

// Standard ReactJS packages
import React, { Component } from "react";
import { Tile } from 'carbon-components-react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { TinyButton as ScrollUpButton } from 'react-scroll-up-button';

// Import pages
import Home from './pages/home';
import About from './pages/about';
import Search from './pages/search';
import NotFound from './pages/notfound';
import { MyVis, YourVis} from './pages/visualization';
import { Hospitals, Illnesses, Charities } from './pages/models';
import { Hospital,  Illness,   Charity }   from './pages/instances';

class App extends Component {
  render() {
    const queryString = require("query-string");
    const parsed = queryString.parse(window.location.search);

    // Define component paths
    let charity_path  = <Route path="/charities" component={Charities} />;
    let hospital_path = <Route path="/hospitals" component={Hospitals} />;
    let illness_path  = <Route path="/illnesses" component={Illnesses} />;
    let search_path = <Route path='/search' component={Search} />;

    if ("id" in parsed){
      charity_path = <Route path="/charities"
          render={props => <Charity {...props} id={parsed.id} />}/>;

      hospital_path = <Route path="/hospitals"
          render={props => <Hospital {...props} id={parsed.id} />}/>;

      illness_path = <Route path="/illnesses"
          render={props => <Illness {...props} id={parsed.id} />}/>;
    }

    if ("value" in parsed)
      search_path = <Route path="/search" 
        render={props => <Search {...props} value={parsed.value}/>}/>;

    return (
      <div className="page">
        <div className="page-body">
          <Router>
            <Switch className="routes">
              <Route exact path="/" component={Home} />
              <Route path="/about" component={About} />
              {illness_path}
              {hospital_path}
              {charity_path}
              {search_path}
              <Route path="/myvis" component={MyVis} />
              <Route path="/yourvis" component={YourVis} />
              <Route component={NotFound} />
            </Switch>
          </Router>
        </div>

        <div>
          <ScrollUpButton style={{backgroundColor: 'transparent', mixBlendMode: 'difference', fill: 'white'}}/>
        </div>

        <Tile className='footer-bg'/>
        <div className="footer">
          <Tile style={{ paddingTop: '25px' }}>
            <span>Mental Health Help
              &copy; {new Date().getFullYear()}: CS 373 Group 11am-9</span>
          </Tile> 
        </div>

      </div>
    );
  }
}

export default App;