import "../css/basic.css";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { visprops } from '../static';
import { Tile, Tab, Tabs, FormLabel, Search, DropdownV2 } from "carbon-components-react";

export class Navigation extends Component {
  render() {
    return (
      <Tile className="navbar-top">
        <div style={{ marginRight: "20px" }}>
          <FormLabel className="navbar-title">
            <Link to='/'>Mental Health Help</Link>
          </FormLabel>
        </div>

        <div style={{ width: "30%"}}>
          <Search labelText="" defaultValue={this.props.value} placeHolderText="Search"
                  onKeyPress={e => {if(e.key === "Enter") window.location.assign("/search?value=" + e.target.value)}}/>
        </div>

        <div style={{marginLeft: 'auto'}}>
          <Tabs className="navbar-tabs" selected={this.props.selected}>
            <Tab label={"Illnesses"} onClick={_ => window.location.assign("/illnesses")}/>
            <Tab label={"Hospitals"} onClick={_ => window.location.assign("/hospitals")}/>
            <Tab label={"Charities"} onClick={_ => window.location.assign("/charities")}/>
            <Tab label={"About"}     onClick={_ => {if(window.location.pathname !== "/about") window.location.assign("/about")}}/>
          </Tabs>
        </div>
        <div>
          <DropdownV2 {...visprops()} onChange={e => {if(window.location.pathname !== e.selectedItem.route) window.location.assign(e.selectedItem.route)}}/>
        </div>
      </Tile>
    );
  }
}