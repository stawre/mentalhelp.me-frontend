import React, { Component } from "react";
import * as d3 from "d3";

export class PieChart extends Component {
  componentDidMount() {
    this.drawChart();
  }

  drawChart() {
    fetch(`http://api.mentalhelp.me/Hospitals`)
      .then(results => results.json())
      .then(dataset => {
        var proprietary = 0;
        var nonprofit = 0;
        var state = 0;
        var notavail = 0;
        //var district = 0;

        dataset.objects.forEach(function(d) {
          if (d.owner === "PROPRIETARY") {
            proprietary++;
          }
          if (d.owner === "NON-PROFIT") {
            nonprofit++;
          }
          if (d.owner === "GOVERNMENT - STATE") {
            state++;
          }
          if (d.owner === "NOT AVAILABLE") {
            notavail++;
          }
          //if(d.owner == "GOVERNMENT - DISTRICT/AUTHORITY"){district++;}
        });
        var data = [
          { label: "PROPRIETARY", value: proprietary },
          { label: "NON-PROFIT", value: nonprofit },
          { label: "GOVERNMENT - STATE", value: state },
          { label: "GOVERNMENT - DISTRICT/AUTHORITY", value: notavail }
          /*,{"label":"NOT AVAILABLE", "value":district}*/
        ];
        console.log(data);

        var width = 960,
          height = 500,
          radius = Math.min(width, height) / 2;

        var color = d3
          .scaleOrdinal()
          .range(["#98abc5", "#8a89a6", "#7b6888", "#606890"]);

        var arc = d3
          .arc()
          .outerRadius(radius - 10)
          .innerRadius(0);

        var labelArc = d3
          .arc()
          .outerRadius(radius - 40)
          .innerRadius(radius - 40);

        var pie = d3
          .pie()
          .sort(null)
          .value(function(d) {
            return d.value;
          });

        var svg = d3
          .select("#area3")
          .append("svg")
          .attr("width", width)
          .attr("height", height)
          .append("g")
          .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var g = svg
          .selectAll(".arc")
          .data(pie(data))
          .enter()
          .append("g")
          .attr("class", "arc");

        g.append("path")
          .attr("d", arc)
          .style("fill", function(d, i) {
            return color(i);
          });

        g.append("text")
          .attr("transform", function(d) {
            return "translate(" + labelArc.centroid(d) + ")";
          })
          .attr("dy", "-0.7em")
          //.attr("dx", "0.7em")
          .attr("text-anchor", "middle")
          .text(function(d, i) {
            return data[i].label;
          });
        g.append("text")
          .attr("transform", function(d) {
            return "translate(" + labelArc.centroid(d) + ")";
          })
          .attr("dy", "0.7em")
          //.attr("dx", "0.7em")
          .attr("text-anchor", "middle")
          .text(function(d, i) {
            return data[i].value;
          });
      });
  }

  render() {
    return (
      <div id="area3" className="vis-area">
        <h2>Psychiatric hospital ownership in the United States</h2><br/><br/>
      </div>
    );
  }
}
