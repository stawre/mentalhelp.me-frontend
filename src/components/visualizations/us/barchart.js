import React, { Component } from "react";
import * as d3 from "d3";

export class BarChart extends Component {
    componentDidMount() {
      this.drawChart();
    }
  
    drawChart() {
      var margin = { top: 20, right: 20, bottom: 150, left: 60 },
      width = 1100 - margin.left - margin.right,
      height = 550 - margin.top - margin.bottom;
  
      // set the ranges
      var x = d3.scaleBand().rangeRound([0, width]).padding(0.05);
  
      var y = d3.scaleLinear().range([height, 0]);
  
      // define the axis
      var xAxis = d3.axisBottom(x);
  
      var yAxis = d3.axisLeft(y).ticks(10);
  
      // add the SVG element
      const svg = d3
      .select("#area1")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
      fetch(`http://api.mentalhelp.me/Illnesses`)
        .then(results => results.json())
        .then(data => {
          data.objects.forEach(function(d) {
          });
          // scale the range of the data
          x.domain(
          data.objects.map(function(d) {
            return d.name;
          })
          );
          y.domain([
          0,
          d3.max(data.objects, function(d) {
            return d.population;
          })
          ]);
  
          // add axis
          svg
          .append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis)
          .selectAll("text")
          .style("text-anchor", "end")
          .attr("dx", "-.8em")
          .attr("dy", "-.55em")
          .attr("transform", "rotate(-90)");
  
          svg
          .append("g")
          .attr("class", "y axis")
          .call(yAxis)
          .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 5)
          .attr("dy", ".71em")
          .style("text-anchor", "end")
          .text("Frequency");
  
          // Add bar chart
          svg
          .selectAll("bar")
          .data(data.objects)
          .enter()
          .append("rect")
          .attr("class", "bar")
          .attr("x", function(d) {
            return x(d.name);
          })
          .attr("width", x.bandwidth())
          .attr("y", function(d) {
            return y(d.population);
          })
          .attr("height", function(d) {
            return height - y(d.population);
          });
        });
    }
  
    render() {
      return (
        <div id="area1" className="vis-area">
          <h2>Number of people affected by common mental conditions in the United States</h2>
        </div>
      );
    }
  }