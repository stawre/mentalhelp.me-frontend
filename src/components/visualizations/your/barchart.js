import '../../../css/basic.css';
import React, { Component } from "react";
import * as d3 from "d3";

export class BarChart extends Component {
  componentDidMount() {
    this.drawChart();
  }

  drawChart() {
    var margin = { top: 20, right: 20, bottom: 50, left: 60 },
      width = 1100 - margin.left - margin.right,
      height = 650 - margin.top - margin.bottom;

    // set the ranges
    var x = d3
      .scaleBand()
      .rangeRound([0, width])
      .padding(0.05);

    var y = d3.scaleLinear().range([height, 0]);

    // define the axis
    var xAxis = d3.axisBottom(x);

    var yAxis = d3.axisLeft(y).ticks(10);

    // add the SVG element
    const svg = d3
      .select("#area1")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var indexes = "";
    for (var a = 1; a <= 209; a++) {
      var line = a + ",";
      indexes += line;
    }
    indexes += "210";
    //console.log(indexes);
    fetch(
      "http://api.isitgettinghotorisitjust.me/country?query=[" + indexes + "]"
    )
      .then(results => results.json())
      .then(data => {
        var count80 = 0;
        var count70 = 0;
        var count60 = 0;
        var count50 = 0;
        var count40 = 0;
        var count30 = 0;
        var count20 = 0;
        var count0  = 0;

        data.results.forEach(function(d) {
          if (d.EPI > 80) {
            count80++;
          } else
          if (d.EPI > 70) {
            count70++;
          } else
          if (d.EPI > 60) {
            count60++;
          } else
          if (d.EPI > 50) {
            count50++;
          } else
          if (d.EPI > 40) {
            count40++;
          } else
          if (d.EPI > 30) {
            count30++;
          } else
          if (d.EPI > 20) {
            count20++;
          } else {
            count0++;
          }
        });

        var dataset = [
          { label: "EPI > 80", value: count80 },
          { label: "80 > EPI > 70", value: count70 },
          { label: "70 > EPI > 60", value: count60 },
          { label: "60 > EPI > 50", value: count50 },
          { label: "50 > EPI > 40", value: count40 },
          { label: "40 > EPI > 30", value: count30 },
          { label: "30 > EPI", value: count20 },
          { label: "Unreported" , value: count0 }
        ];
        //console.log(dataset);
        //console.log(data.results);

        // scale the range of the data
        x.domain(
          dataset.map(function(d) {
            return d.label;
          })
        );
        y.domain([
          0,
          d3.max(dataset, function(d) {
            return d.value;
          })
        ]);

        // add axis
        svg
          .append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis)
          .selectAll("text")
          .style("text-anchor", "end")
          .attr("dx", "-.8em")
          //.attr("dy", "-.55em")
          .attr("transform", "rotate(0)");

        svg
          .append("g")
          .attr("class", "y axis")
          .call(yAxis)
          .append("text")
          .attr("transform", "rotate(-90)")
          .attr("y", 5)
          .attr("dy", ".71em")
          .style("text-anchor", "end")
          .text("Frequency");

        // Add bar chart
        svg
          .selectAll("bar")
          .data(dataset)
          .enter()
          .append("rect")
          .attr("class", "bar")
          .attr("x", function(d) {
            return x(d.label);
          })
          .attr("width", x.bandwidth())
          .attr("y", function(d) {
            return y(d.value);
          })
          .attr("height", function(d) {
            return height - y(d.value);
          });
      });
  }

  render() {
    return (
      <div id="area1" className="vis-area">
        <h2>Number of countries by EPI</h2><br/><br/>
      </div>
    );
  }
}
