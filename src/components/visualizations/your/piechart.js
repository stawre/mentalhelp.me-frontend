import React, { Component } from "react";
import * as d3 from "d3";

export class PieChart extends Component {
  componentDidMount() {
    this.drawChart();
  }

  drawChart() {
    var indexes = "";
    for (var a = 1; a <= 442; a++) {
      var line = a + ",";
      indexes += line;
    }
    indexes += "443";
    //console.log(indexes);
    fetch(
      "http://api.isitgettinghotorisitjust.me/charity?query=[" + indexes + "]")
      .then(results => results.json())
      .then(data => {
        var count20 = 0;
        var count15 = 0;
        var count10 = 0;
        var count5  = 0;
        var count0  = 0;
        //var district = 0;
        console.log(data);
        data.results.forEach(function(d) {
          // if (d.country.length > 35) {
          //   count35++;
          // } else
          // if (d.country.length > 30) {
          //   count30++;
          // } else
          // if (d.country.length > 25) {
          //   count25++;
          // } else
          if (d.country.length > 20) {
            count20++;
          } else
          if (d.country.length > 15) {
            count15++;
          } else
          if (d.country.length > 10) {
            count10++;
          } else
          if (d.country.length > 5) {
            count5++;
          } else {
            count0++;
          } 
        });
        var dataset = [
          { label: "> 20 countries", value: count20 },
          { label: "15-20 countries", value: count15 },
          { label: "10-15 countries", value: count10 },
          { label: "5-10 countries" , value: count5 },
          { label: "< 5 countries" , value: count0 }
        ];
        console.log(data);

        var width = 960,
          height = 500,
          radius = Math.min(width, height) / 2;

        var color = d3
          .scaleOrdinal()
          .range(["#98abc5", "#8a89a6", "#7b6888", "#606890", "#606080"]);

        var arc = d3
          .arc()
          .outerRadius(radius - 10)
          .innerRadius(0);

        var labelArc = d3
          .arc()
          .outerRadius(radius - 40)
          .innerRadius(radius - 40);

        var pie = d3
          .pie()
          .sort(null)
          .value(function(d) {
            return d.value;
          });

        var svg = d3
          .select("#area3")
          .append("svg")
          .attr("width", width)
          .attr("height", height)
          .append("g")
          .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var g = svg
          .selectAll(".arc")
          .data(pie(dataset))
          .enter()
          .append("g")
          .attr("class", "arc");

        g.append("path")
          .attr("d", arc)
          .style("fill", function(d, i) {
            return color(i);
          });

        g.append("text")
          .attr("transform", function(d) {
            return "translate(" + labelArc.centroid(d) + ")";
          })
          .attr("dy", "-0.7em")
          //.attr("dx", "0.7em")
          .attr("text-anchor", "middle")
          .text(function(d, i) {
            return dataset[i].label;
          });
        g.append("text")
          .attr("transform", function(d) {
            return "translate(" + labelArc.centroid(d) + ")";
          })
          .attr("dy", "0.7em")
          //.attr("dx", "0.7em")
          .attr("text-anchor", "middle")
          .text(function(d, i) {
            return dataset[i].value;
          });
      });
  }

  render() {
    return (
      <div id="area3" className="vis-area">
        <h2>Number of charities that support a range of countries</h2>
        <br />
        <br />
      </div>
    );
  }
}
