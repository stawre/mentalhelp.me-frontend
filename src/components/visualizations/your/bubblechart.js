import '../../../css/basic.css';
import React, { Component } from "react";
import * as d3 from "d3";

export class BubbleChart extends Component {
  componentDidMount() {
    this.drawChart();
  }

  drawChart() {
    var indexes = "";
    for (var a = 1; a <= 14; a++) { //292
      var line = a + ",";
      indexes += line;
    }
    indexes += "15";
    fetch(
      "http://api.isitgettinghotorisitjust.me/issue?query=[" + indexes + "]"
    )
      .then(results => results.json())
      .then(data => {
        var dataset = { children: data.results };

        console.log(dataset);

        var diameter = 1100;
        var colors = [];
        while (colors.length < 50) {
          do {
            var color = Math.floor(Math.random() * 1000000 + 1);
          } while (colors.indexOf(color) >= 0);
          colors.push("#" + ("000000" + color.toString(16)).slice(-6));
        }

        var bubble = d3
          .pack(dataset)
          .size([diameter, diameter])
          .padding(1.5);

        var svg = d3
          .select("#area2")
          .append("svg")
          .attr("width", diameter)
          .attr("height", diameter)
          .attr("class", "bubble");

        //var csize = d.countries.foreach(function(d))

        var nodes = d3.hierarchy(dataset).sum(function(d) {
          //console.log(d.countries.length);
          if(d.countries && d.countries.length)
          {
            console.log(d.countries.length);
            return d.countries.length;
          }
          if (undefined !== d.countries && d.countries.length) {
            return d.countries.length;
          } else {
            return 2;
          }
          //console.log(d.countries.length);
          //return 10;
        });

        var node = svg
          .selectAll(".node")
          .data(bubble(nodes).descendants())
          .enter()
          .filter(function(d) {
            return !d.children;
          })
          .append("g")
          .attr("class", "node")
          .attr("transform", function(d) {
            return "translate(" + d.x + "," + d.y + ")";
          });

        node.append("title").text(function(d) {
          return d.data.countries.length;
        });

        node
          .append("circle")
          .attr("r", function(d) {
            return d.r;
          })
          .style("fill", function(d, i) {
            return colors[i];
          });

        node
          .append("text")
          .attr("dy", ".2em")
          .style("text-anchor", "middle")
          .text(function(d) {
            return d.data.issue_name.substring(0, d.r / 3);
          })
          .attr("font-family", "sans-serif")
          .attr("font-size", function(d) {
            return d.r / 10;
          })
          .attr("fill", "white");

        node
          .append("text")
          .attr("dy", "1.3em")
          .style("text-anchor", "middle")
          .text(function(d) {
            console.log(d);
            return d.data.countries.length;
          })
          .attr("font-family", "Gill Sans", "Gill Sans MT")
          .attr("font-size", function(d) {
            return d.r / 5;
          })
          .attr("fill", "white");

        d3.select(window.frameElement).style("height", diameter + "px");
      });
  }

  render() {
    return (
      <div id="area2" className="vis-area">
        <h2>Most important issues affecting countries</h2><br/><br/>
      </div>
    );
  }
}
